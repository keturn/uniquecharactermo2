"""Update your Unique Character with copies of the current meshes and textures.

A plugin for Mod Organizer 2 for the Unique Character mod:
https://www.nexusmods.com/skyrimspecialedition/mods/3006

Replaces the Unique Character Tool batch file:
https://www.nexusmods.com/skyrimspecialedition/mods/11080

This is a very blunt instrument, made with scant of how Unique Character works or how mesh and
texture files are loaded. Suggestions are welcome!

TODO:

+ Add more species that are supported by the mod to some degree but left out of the batch file:
  - khajiit and argonian (with MF and Vampire variants)
  - vampire lord
  - werewolf
+ A user interface. (Did it do anything? Is it done? What did it do?)
+ Tell MO2 about the outputs so it doesn't yell at the user about files in Overwrites.
"""
import base64
import os
import re
import shutil
from dataclasses import dataclass
from enum import Enum
from itertools import chain
from pathlib import Path, PurePath
from typing import List, Optional

import PyQt5.QtGui
import mobase
from PyQt5.QtCore import qDebug
from PyQt5.QtGui import QPixmap
from mobase import IOrganizer, PluginSetting, VersionInfo


class MF(Enum):
    FEMALE = "female"
    MALE = "male"

    @classmethod
    def match(cls, s: str):
        if re.search("female", s, re.IGNORECASE):
            return cls.FEMALE
        elif re.search("male", s, re.IGNORECASE):
            return cls.MALE
        else:
            return None


class BodyPart(Enum):
    BODY = "body"
    FEET = "feet"
    HANDS = "hands"
    HEAD = "head"

    @classmethod
    def match(cls, s):
        return next((part for part in cls if re.search(part.value, s, re.IGNORECASE)), None)


class Race(Enum):
    """Values match the Unique Character directory names."""
    BRETON = "Breton"
    DARK_ELF = "Darkelf"
    HIGH_ELF = "Highelf"
    IMPERIAL = "Imperial"
    NORD = "Nord"
    ORC = "Orc"
    REDGUARD = "Redguard"
    WOOD_ELF = "Woodelf"

    @classmethod
    def match(cls, s: str):
        return next((race for race in cls if re.search(race.value, s, re.IGNORECASE)), None)


@dataclass
class CharacterAssetInfo:
    mf: Optional[MF] = None
    bodyPart: Optional[BodyPart] = None
    race: Optional[Race] = None
    vampire: bool = False


MESH_SOURCE = Path("meshes/actors/character/character assets/")
MESH_DEST = Path("meshes/actors/character/unique/")

TEXTURE_SOURCE = Path("textures/actors/character/")
TEXTURE_DEST = Path("textures/actors/character/unique/")


def sort_mesh(filename) -> CharacterAssetInfo:
    return CharacterAssetInfo(
        MF.match(filename),
        BodyPart.match(filename),
        vampire=bool(re.search("vampire", filename, re.IGNORECASE))
    )


def sort_texture(filename) -> CharacterAssetInfo:
    info = CharacterAssetInfo(
        mf=MF.match(filename),
        race=Race.match(filename),
        vampire=bool(re.search("vampire", filename, re.IGNORECASE))
    )
    if re.match("blankdetailmap", filename, re.IGNORECASE):
        info.bodyPart = BodyPart.HEAD
    else:
        info.bodyPart = BodyPart.match(filename)
    return info


def mesh_destinations(filename) -> List[Path]:
    # copy *female* to dest/female and dest/Vampire/female
    # copy *male* (excluding *female*) to dest/male and dest/Vampire/male

    # if "head", copy to dest/$mf/Head/{all races}/

    info = sort_mesh(filename)

    destinations = []

    races = [info.race] if info.race else Race
    alters = ["Vampire"] if info.vampire else [".", "Vampire"]
    mfs = [info.mf] if info.mf else MF

    if info.bodyPart == BodyPart.HEAD or info.bodyPart is None:
        for alter in alters:
            for mf in mfs:
                for race in races:
                    destinations.append(Path(MESH_DEST, alter, mf.value, "Head", race.value))

    if info.bodyPart != BodyPart.HEAD or info.bodyPart is None:
        for alter in alters:
            for mf in mfs:
                destinations.append(Path(MESH_DEST, alter, mf.value))

    return destinations


def texture_destinations(filename) -> List[Path]:
    info = sort_texture(filename)

    destinations = []
    # dest/$mf/{body, feet, hands}
    # dest/$mf/head/$race/

    races = [info.race] if info.race else Race
    alters = ["Vampire"] if info.vampire else [".", "Vampire"]
    mfs = [info.mf] if info.mf else MF

    # FIXME: Does it help at all to copy these textures that don't match a body part?
    #    I have no idea! I threw it in here to be inclusive, but maybe it's ignored in the end.
    body_parts = [info.bodyPart] if info.bodyPart else [BodyPart.BODY, BodyPart.HEAD]

    for alter in alters:
        for mf in mfs:
            for body_part in body_parts:
                if body_part == BodyPart.HEAD:
                    # Only heads have race subdirectories.
                    for race in races:
                        destinations.append(Path(TEXTURE_DEST, alter, mf.value, "Head", race.value))
                else:
                    destinations.append(Path(TEXTURE_DEST, alter, mf.value, body_part.value))

    return destinations


def is_relative_to(path: PurePath, base: PurePath) -> bool:
    # backport for Python 3.9 function
    try:
        path.relative_to(base)
    except ValueError as e:
        return False
    return True


class UniqueCharacterTool(mobase.IPluginTool):
    _organizer: IOrganizer = None
    dryRun = False

    # 32x32 PNG data
    _iconData = base64.b85decode(
        b'iBL{Q4GJ0x0000DNk~Le0000W0000W1Oos70D)9z#{d8TFi=cXMU9P({{H^`{rv#}0imIx+}zym?(XmJ@AUNa?d|'
        b'PdU0p#zK`SdOy1Kf1dwUiZ7O9$l%K!iX5Oh*bQv-tjRsK0+gtq8}vKz>-eT5ZoPyhe`_en%SRCobo$bkvKFbD*|'
        b'Ko&^dK)ZnQD|G*vYB3b@euDqpXp#J$x5|H549XyjAd~>IPVi8IDFh>IgI=|!6il<b)`kHfIT!q^p2yGE0hD('
        b'TePQCM3=E+G46LCLe#9gt%~A%TsSF$%3qX9fNlG)7Gy;TXAK=j30}_Z-y7%s;<{N}Slgj4*zwZ5k3$RX_ZNtF)'
        b'`GC;ev`nbL{R80eS5i`f2q;}<Koyt-6`1nwC0KxG(kzIU+5dk)Y$?(RU|?%IfUtIj()6@WWOwj20gaox1|h(+'
        b'cBazI_Xe5W2iWI=1sKlnd%fR=;naQxp5K1}BdTICa98-%00000NkvXXu0mjf')

    def init(self, organizer: IOrganizer) -> bool:
        self._organizer = organizer
        return True

    def display(self):
        mesh_files = self._organizer.findFileInfos(str(MESH_SOURCE), lambda x: True)
        destinations = [(input_file, mesh_destinations(input_file.filePath))
                        for input_file in mesh_files]

        if not self.dryRun:
            for input_file, outputs in destinations:
                self.copyFile(input_file, outputs)

        # do the same for textures
        texture_directories = self._organizer.listDirectories(str(TEXTURE_SOURCE))

        def exclude_output(file_info):
            """careful: TEXTURE_DEST is a subdirectory of SOURCE; must filter it out!"""
            return not is_relative_to(Path(file_info.filePath), TEXTURE_DEST)

        texture_files = list(chain.from_iterable(
            self._organizer.findFileInfos(str(Path(TEXTURE_SOURCE, d)), exclude_output)
            for d in texture_directories))

        destinations = [(input_file, texture_destinations(input_file.filePath))
                        for input_file in texture_files]

        if not self.dryRun:
            for input_file, outputs in destinations:
                self.copyFile(input_file, outputs)

    def copyFile(self, source: mobase.FileInfo, destinations):
        overwrite = Path(self._organizer.overwritePath())
        for d in destinations:
            name = Path(source.filePath).name
            out_dir = Path(overwrite, d)
            os.makedirs(out_dir, exist_ok=True)
            shutil.copy2(source.filePath, Path(out_dir, name))
            qDebug("copied %s to %s" % (name, out_dir))

    def displayName(self) -> str:
        return "Unique Character Tool"

    def tooltip(self) -> str:
        return "Copy current assets to Unique Character"

    def author(self) -> str:
        return "keturn"

    def description(self) -> str:
        return "Support for Skyrim Unique Character"

    def settings(self) -> List["PluginSetting"]:
        return []

    def version(self) -> "VersionInfo":
        return VersionInfo(1, 0, 0)

    def name(self):
        return "uniqueCharacterTool"

    def icon(self) -> PyQt5.QtGui.QIcon:
        pixmap = QPixmap(32, 32)
        pixmap.loadFromData(self._iconData, 'PNG')
        return PyQt5.QtGui.QIcon(pixmap)


def createPlugin() -> mobase.IPlugin:
    return UniqueCharacterTool()
